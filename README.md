
# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Antecedentes ENIAC](capitulos/Antecedentes-Eniac.md)
2. [Evolução](capitulos/evolução.md)
3. [ENIAC Características Gerais](capitulos/eniac-características-gerais.md)
4. [ENIAC Curiosidades](capitulos/ENIAC-curiosidades.md)
5. [Importância](capitulos/Importância.md)





## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| 
![](https://gitlab.com/uploads/-/system/user/avatar/11481792/avatar.png?width=400)  | Maycon Antônio  | mayconantonio | [mayconc.2022@utfpr.edu.br](mailto:mayconc.2022@utfpr.edu.br)
![](https://secure.gravatar.com/avatar/3908c185b78d658d430988d21fd4e42c?s=400&d=identicon)  | Jean Mario   | Jmario120 | [jeanmario@alunos.utfpr.edu.br](mailto:jeanmario@alunos.utfpr.edu.br)
![](https://secure.gravatar.com/avatar/f165567d46eebabd4f9c5946ce7c1cee?s=400&d=identicon)  | Tarcisio Oro  | n3wsty | [niehues@alunos.utfpr.edu.br](mailto:niehues@alunos.utfpr.edu.br)
![](https://secure.gravatar.com/avatar/6e136f9a82e0623b8d769579b732e8ea?s=400&d=identicon)  | Caiuã Brandão  | caiua_mello | [caiua@alunos.utfpr.edu.br](mailto:caiua@alunos.utfpr.edu.br)
![](https://gitlab.com/uploads/-/system/user/avatar/11692434/avatar.png?width=400)  | Gabriel  Bezerra  | OBzrra | [gmourino@alunos.utfpr.edu.br](mailto:gmourino@alunos.utfpr.edu.br)
