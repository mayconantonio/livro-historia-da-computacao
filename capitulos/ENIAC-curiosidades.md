# ENIAC Curiosidades

O ENIAC (Electronic Numerical Integrator and Computer) em português conhecido como Computador
Integrador Numérico Eletrônico.

Eniac foi o primeiro computador do mundo, criado pela universidade da Pensilvânia para calcular trajetórias
balísticas a não tanto tempo, 74 anos atrás, foi lançado em 1946, a máquina era gigantesca e pesava cerca de 30
toneladas, e ela ocupava um espaço de 180 metros quadrados. Na época, o exército norte americano gastou
cerca de quinhentos mil dólares com ele, oque equivale hoje cerca de seis milhões de dólares.

As mentes atrás da criação do ENIAC foram os pesquisadores John Mauchly e J. Presper Eckert, mas eles
não fizeram tudo sozinhos, havia uma equipe enorme trabalhando duro para que essa máquina se tornasse
possível. E levaram cerca de dois anos para isso acontecer.

Mas para tudo isso acontecer, o computador utilizava de um hardware de mais de 70 mil resistores e mais de 18
mil válvulas a vácuo, isso rendia uma conta de energia bem alta, ele consumia
 cerca de 200 mil watts de energia.

 A maquina em si, era composta por muito painéis individuais, e cada um desses painéis realizava uma
operação diferente ao mesmo tempo. A linha de raciocínio dela era enviar os números e receber entre sí, fazer os cálculos necessários, salvar os resultados e desencadear a próxima operação.

Para criar o ENIAC do zero, foram preciso muitos programadores, mas o que muitos não sabem é que a maior
parte desses programadores eram mulheres, e foram de uma importância imensa para a criação do projeto, mas
mesmo assim elas não receberam o mérito por tal obra, que não era nem um pouco fácil, para levar um problema para
ser mapeado no computador, levava cerca de semanas.

E depois de um tempo, foram surgindo novas tecnologias e o ENIAC tornou-se obsoleto, e decidiram
desligar a maquina no dia 2 de outubro de 1955, e partes da maquina se encontram em museus de ciência do mundo
todo.

Mesmo o seu tempo de atuação tenha sido pouco, o ENIAC realizou muitas coisas importantes para a época,
embora a maior parte das coisas eram sigilosas, sabe-se que ele foi usado depois da segunda guerra mundial para
desenvolver a bomba nuclear que foi usada contra Hiroshima e Nagazaki.


# Referências bibliográficas:

1.<https://www.hipercultura.com/historia-primeiro-computador/#:~:text=Foi%20desenvolvido%20por%20John%20Eckert%20e%20John%20Mauchly,hidrog%C3%AAnio.%20Os%20cientistas%20norte-americanos%20que%20desenvolveram%20o%20ENIAC>

2.<https://segredosdomundo.r7.com/eniac/>
