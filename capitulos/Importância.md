# Importância do ENIAC para o mundo

Após todos esses anos desde sua criação, o ENIAC ainda representa um grande avanço para a engenharia, a computação e para a história. Antes dele, os computadores existentes eram inteiramente e efetivamente
manuseados por humanos, sendo muitas vezes necessário horas, ou dias,de trabalho e atenção para serem devidamente utilizados, como a máquina de Turing. O ENIAC foi idealizado para minimizar este tempo, dando assim
origem aos computadores elétricos e automatizados.

Apesar de ser utilizado por poucos anos e rapidamente substituído por máquinas mais potentes e efetivas, sua criação foi um grande marco para a evolução tecnológica, iniciando-se assim uma corrente de evolução
rápida nas áreas eletrônicas. O EDIVAC (Eletronic Discrete Variable Automatic Computer) e ORDVAC (Ordnance Discrete Variable Automatic Computer) foram sucessores diretos do ENIAC, desenvolvidos a fim de
melhorar e atualizar os problemas e defeitos de seu antecessor.

O ENIAC também foi muito importante na inserção e normalização
das mulheres em áreas da engenharia. Grande parte dos programadores da
época eram mulheres e ajudavam ativamente nesses projetos. Sobre responsabilidade do ENIAC participavam 6 mulheres. Infelizmente, só após quase 50 anos desde a criação do computador que as programadoras
responsáveis foram devidamente reconhecidas.

# Referências bibliográficas:
1.<https://www.youtube.com/watch?v=dy0wpDfnpzo&ab_channel=TecMundo>
