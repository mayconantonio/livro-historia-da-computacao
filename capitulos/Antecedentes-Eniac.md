# Antecedentes do ENIAC

Em 1642 o matemático e filósofo francês, inventou a primeira máquina automática
de cálculo, a Pascalina, sendo essa a evolução do Ábaco, ela era capaz de realizar
apenas soma e subtração, no qual o resultado era exibido em uma sequência de janelinhas.

A máquina que foi a primeira a realizar sem dificuldade subtração, multiplicação e
divisão, raiz quadrada foi construída em volta de 1672 pelo matemático, filósofo Gottfried
Wilhelm Von Leibniz. Sua máquina  foi chamada de calculadora universal e foi o
aperfeiçoamento da Pascalina.

Em 1822, o professor de Cambridge e matemático Charles Babbage, idealizou a
Máquina Das Diferenças, um dispositivo mecânico baseado em rodas dentadas que poderia
computar e imprimir extensas tabelas científicas, a máquina nunca foi construída devido às
limitações tecnológicas da época e altos custos.

Em 1833,projetou a Máquina Analítica, foi concebida para executar uma grande
gama de tarefas de cálculos, de acordo com as instruções oferecidas pelo seu operador, por
cartões perfurados. A Máquina Analítica nunca foi construída, no entanto graças a esse
projeto ficou conhecido como o Pai da informática, porquanto esse projeto estava muito à
frente da época e tinha muitas características dos computadores atuais.


# Referências bibliográficas:

1.<https://www.tecmundo.com.br/tecnologia-da-informacao/1697-a-historia-dos-computadores-e-da-computacao.htm>
