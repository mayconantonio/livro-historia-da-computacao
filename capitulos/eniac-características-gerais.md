# ENIAC Características Gerais

Em 1945, sob encomenda do exército dos Estados Unidos, foi desenvolvido um computador
sem partes mecânicas “ENIAC” seu propósito era calcular trajetórias balísticas de lançamento de
mísseis, algo muito difícil de se fazer a mão.

O ENIAC era operado com cartões perfurados e precisava de uma equipe enorme de
operadoras, propriamente as programadoras dele. Começou a ser usado no fim de 1945, por não ter
sido completado durante a guerra, foi movido e religado em 1947, permanecendo em uso até 1955.
Nesse tempo foi usado para fazer cálculos necessários para a bomba de hidrogênio.

Logo após o ENIAC veio o EDVAC, e ao contrário do seu "irmão", usava código binário e
também foi desenvolvido para aplicações militares, como cálculo balístico.

# Referências bibliográficas:

1.<https://tecnoblog.net/meiobit/410202/primeiro-computador-origem-historia/>
